//
//  BusinessCardListTableViewCell.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 02/12/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class BusinessCardListTableViewCell: UITableViewCell {

    @IBOutlet weak var cardImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
