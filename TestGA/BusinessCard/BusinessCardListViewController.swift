//
//  BusinessCardListViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 02/12/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit
import ALCameraViewController

class BusinessCardListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    var cards = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.register(R.nib.businessCardListTableViewCell(), forCellReuseIdentifier: R.reuseIdentifier.cardListCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.addButton.layer.cornerRadius = self.addButton.frame.width / 2
    }

    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        /// Provides an image picker wrapped inside a UINavigationController instance
        let imagePickerViewController = CameraViewController(croppingParameters: CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: CGSize.zero), allowsLibraryAccess: true, allowsSwapCameraOrientation: true, allowVolumeButtonCapture: true) { (image, _) in
            if let image = image {
                self.cards.append(image)
                self.tableView.reloadData()
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        
        present(imagePickerViewController, animated: true, completion: nil)
        
    }
}

extension BusinessCardListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cardListCell.identifier, for: indexPath) as! BusinessCardListTableViewCell
        cell.cardImageView.image = self.cards[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cards.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.width / 1.586
    }
    
}
