//
//  BusinessCardViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 13/11/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit
import SegueManager

class BusinessCardViewController: UIViewController, SeguePerformer {
    
    lazy var segueManager: SegueManager = {
        // SegueManager based on the current view controller
        return SegueManager(viewController: self)
    }()
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var addButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.collectionView.register(R.nib.homeCollectionViewCell)
        self.addButton.layer.cornerRadius = self.addButton.bounds.size.width / 2
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segueManager.prepare(for: segue)
    }
    
}

extension BusinessCardViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.homeCell, for: indexPath)!
        switch indexPath.item {
        case 0:
            cell.titleLabel.text = "Travel Contact"
        case 1:
            cell.titleLabel.text = "Uni"
        case 2:
            cell.titleLabel.text = "HK\nSIN\nMALAY"
        case 3:
            cell.titleLabel.text = "USA"
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: R.segue.businessCardViewController.categorySegue) { (segue) in
            let cell = collectionView.cellForItem(at: indexPath) as! HomeCollectionViewCell
            segue.destination.title = cell.titleLabel.text
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width - 48) / 2, height: (self.view.bounds.width - 48) / 2)
    }
}
