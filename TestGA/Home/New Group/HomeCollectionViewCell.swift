//
//  HomeCollectionViewCell.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 05/11/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
