//
//  HomeViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 05/11/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print()
        // Do any additional setup after loading the view.
        self.collectionView.register(R.nib.homeCollectionViewCell)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "Home"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.homeCell, for: indexPath)!
        switch indexPath.item {
        case 0:
            cell.titleLabel.text = "Business Card"
        case 1:
            cell.titleLabel.text = "Money Management"
        case 2:
            cell.titleLabel.text = "What's New"
        case 3:
            cell.titleLabel.text = "Member Card Wallet"
        case 4:
            cell.titleLabel.text = "Receipt"
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width - 48) / 2, height: (self.view.bounds.width - 48) / 2) 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            self.performSegue(withIdentifier: R.segue.homeViewController.businessCardSegue, sender: self)
        case 1:
            self.performSegue(withIdentifier: R.segue.homeViewController.moneyManagementSegue, sender: self)
        case 2:
            self.performSegue(withIdentifier: R.segue.homeViewController.newsFeedSegue, sender: self)
        case 3:
            self.performSegue(withIdentifier: R.segue.homeViewController.memberCardWalletSegue, sender: self)
        case 4:
            self.performSegue(withIdentifier: R.segue.homeViewController.receiptSegue.identifier, sender: self)
        default:
            return
        }
    }
    
}
