//
//  BillViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 10/12/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class BillViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.register(R.nib.billTableViewCell(), forCellReuseIdentifier: R.reuseIdentifier.billCell.identifier)
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title = "Bill"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BillViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.billCell, for: indexPath)!
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "DBS Credit Card"
        case 1:
            cell.titleLabel.text = "Rent, Starhub, SP"
        case 2:
            cell.titleLabel.text = "Italy Bank Card"
        default:
            break
        }
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_GB")
        formatter.setLocalizedDateFormatFromTemplate("MMMMd")
        cell.dateLabel.text = formatter.string(from: date)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return BillSectionHeader(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}
