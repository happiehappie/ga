//
//  BillSectionHeader.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 10/12/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class BillSectionHeader: UIView {

    @IBOutlet var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(R.nib.billSectionHeader.name, owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = UIColor.green
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
}
