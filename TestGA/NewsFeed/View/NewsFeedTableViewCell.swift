//
//  NewsFeedTableViewCell.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 26/11/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var timeAgoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
