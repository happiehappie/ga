//
//  NewsFeedViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 26/11/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class NewsFeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationItem.title = "What's New"
        self.tableView.register(R.nib.newsFeedTableViewCell(), forCellReuseIdentifier: R.nib.newsFeedTableViewCell.identifier)
        self.tableView.tableFooterView = UIView()
    }

}

extension NewsFeedViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.newsFeedCell.identifier, for: indexPath) as! NewsFeedTableViewCell
        cell.titleLabel.text = "Neque porro quisquam"
        cell.subtitleLabel.text = "Mauris tristique augue in leo semper, sed pellentesque sem finibus. Proin feugiat dui vel eros accumsan, eu venenatis felis posuere. In a dui est. Aenean vehicula orci pharetra orci pulvinar, eu molestie diam efficitur. Donec accumsan at tortor a feugiat. Donec rutrum feugiat ligula at egestas. Nam sodales ipsum pharetra, interdum sapien eu, faucibus mauris. Proin nec pulvinar justo. Donec mattis neque non augue suscipit, vitae lacinia nunc fermentum. Sed id ligula vitae dolor suscipit commodo."
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

