//
//  MoneyManagementViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 26/11/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit
import SegueManager

class MoneyManagementViewController: UIViewController {
    
    lazy var segueManager: SegueManager = {
        // SegueManager based on the current view controller
        return SegueManager(viewController: self)
    }()
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.register(R.nib.homeCollectionViewCell)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segueManager.prepare(for: segue)
    }
    
}


extension MoneyManagementViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.homeCell, for: indexPath)!
        
        switch indexPath.item {
        case 0:
            cell.titleLabel.text = "Bills"
        case 1:
            cell.titleLabel.text = "Investment"
        case 2:
            cell.titleLabel.text = "Saving Plan"
        case 3:
            cell.titleLabel.text = "Calendar"
        default:
            break
        }
        return cell
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width - 48) / 2, height: (self.view.frame.width - 48) / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            
            self.performSegue(withIdentifier: R.segue.moneyManagementViewController.billSegue.identifier, sender: self)
        case 1:
            self.performSegue(withIdentifier: R.segue.moneyManagementViewController.investmentSegue.identifier, sender: self)
        default:
            break
        }
    }
    
}
