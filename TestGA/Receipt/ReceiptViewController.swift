//
//  ReceiptViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 24/12/17.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class ReceiptViewController: UIViewController {

    @IBOutlet weak var addButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addButton.layer.cornerRadius = 25
        self.navigationItem.title = "Receipt"
    }

}
