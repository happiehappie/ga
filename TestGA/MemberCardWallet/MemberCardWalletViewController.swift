//
//  MemberCardWalletViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 17/12/2017.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit

class MemberCardWalletViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var snapACardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.collectionView.register(R.nib.homeCollectionViewCell)
        self.snapACardButton.titleLabel?.textAlignment = .center
        self.navigationItem.title = "Member Wallet/Loyalty"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MemberCardWalletViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.homeCell, for: indexPath)!
        switch indexPath.item {
        case 0:
            cell.titleLabel.text = "Airline\nHotel"
        case 1:
            cell.titleLabel.text = "Supermarket"
        case 2:
            cell.titleLabel.text = "Resto"
        case 3:
            cell.titleLabel.text = "+"
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.bounds.width - 48) / 2, height: (self.view.bounds.width - 48) / 2)
    }
}
