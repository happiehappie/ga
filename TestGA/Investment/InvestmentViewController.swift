//
//  InvestmentViewController.swift
//  TestGA
//
//  Created by Jack Xiong Lim on 24/12/17.
//  Copyright © 2017 Jack Xiong Lim. All rights reserved.
//

import UIKit
import SegueManager

class InvestmentViewController: UIViewController {
    
    lazy var segueManager: SegueManager = {
        // SegueManager based on the current view controller
        return SegueManager(viewController: self)
    }()
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.collectionView.register(R.nib.homeCollectionViewCell)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "Investment"
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segueManager.prepare(for: segue)
    }
    
}

extension InvestmentViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.homeCell, for: indexPath)!
        
        switch indexPath.item {
        case 0:
            cell.titleLabel.text = "Currency\nAlert"
        case 1:
            cell.titleLabel.text = "Stock"
        case 2:
            cell.titleLabel.text = "Insurance"
        case 3:
            cell.titleLabel.text = "Fund\nBond"
        default:
            break
        }
        return cell
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width - 48) / 2, height: (self.view.frame.width - 48) / 2)
    }

}
